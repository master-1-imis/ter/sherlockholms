#!/usr/bin/python3

##############################################################################
# IMPORTS																	 #
##############################################################################
from typing import List, Tuple
import sys



##############################################################################
# FUNCTION																	 #
##############################################################################
def dataFilter(path_file_in:str, path_file_out:str, path_file_select:str):
	"""Génère un nouveau fichier depuis un fichier source avec pour différence un filtrage de données."""
	with open(file=path_file_out, mode="w", encoding="utf-8") as file_out:
		with open(file=path_file_in, mode="r", encoding="utf-8") as file_in:
			with open(file=path_file_select, mode="r", encoding="utf-8") as file_select:
				data_to_select = set(map(lambda r:r.split(";")[0][:-1], file_select.readlines()))
				data_in = file_in.readlines()
				file_out.write(data_in[0])
				for row in data_in:
					if row.split(";")[0] in data_to_select:
						file_out.write(row)


##############################################################################
# RUN																		 #
##############################################################################
if __name__ == "__main__":
	arg_number = len(sys.argv)

	path_file_in = str()
	path_file_out = str()
	path_file_select = str()

	# récupération du fichier csv d'entrée
	if arg_number == 1:
		path_file_in = input("File in: ")
	else:
		path_file_in = sys.argv[1]
	
	# récupération du fichier csv de sortie
	if arg_number <= 2:
		path_file_out = input("File out: ")
	else:
		path_file_out = sys.argv[2]
	
	# récupération du fichier csv de sortie
	if arg_number <= 3:
		path_file_select = input("File to select: ")
	else:
		path_file_select = sys.argv[3]

	# filtrage des données
	dataFilter(
		path_file_in=path_file_in,
		path_file_out=path_file_out,
		path_file_select=path_file_select
	)