# Script bash

L'objectif de ces scripts est de vérifier si une application est un virus ou pas. Cela ce réalise en 3 étapes:  

| Étape | Script | Entrée | Sortie | Plateforme | Description |
| :-:   | :--    | :--    | :--    | :--        | :--         |
| 1 | `apkExist.sh` | Une liste d'application. | Une liste d'application disponnible et une autre pour les non disponnible. | [Google Play](https://play.google.com/store/apps/details?id=) | Vérifie que les applications sont encore disponnibles sur le store. |
| 2 | `urlDownloader.sh` | Une liste d'application. | Une liste d'application téléchargeable. | [Evozi](https://apps.evozi.com/apk-downloader/) | Récupère le lien de téléchargement des applications. |
| 3 | `analyzeDownloader.sh` | Une liste d'application avec le lien de téléchargement | Une liste d'application avec une classe d'appartenance. | [Virustotal](https://www.virustotal.com/gui/home/upload) | Teste à travers une api si l'application est un malware.

<br/><br/>


<!-------------------- Étape 1 -------------------->
# Étape 1
## Exécution
### Syntaxe
```sh
apkExist.sh FICHIER_SOURCE [LIGNE_DE_DEBUT]
```

### Description
Liste les applications de `FICHIER_SOURCE` qui sont encore disponnibles sur Google Play, à partir de la `LIGNE_DE_DEBUT` (ligne 1 par défaut).

### Exemples
```sh
$ ./apkExist.sh data/csv/SourceInconnue.csv
$ ./apkExist.sh data/csv/SourceInconnue.csv 10
```

## Fichiers générés
| Catégorie | Reptoire | Fichiers | Description |
| :--       | :--      | :--      | :--         |
| log | `/var/log/sherlockholms` | `apkExist.log` | Journal des événements. |
| resultat | `sherlockholms/data/store/exist/googleplay` | `GooglePlayStore_errors.csv` | Applications pour lequel une erreur est survenue lors de la recherche. |
| resultat | `sherlockholms/data/store/exist/googleplay` | `GooglePlayStore_notfound.csv` | Applications plus disponnibles sur Google Play. |
| resultat | `sherlockholms/data/store/exist/googleplay` | `GooglePlayStore_success.csv` | Application encore disponnibles sur Google Play. |

## Requête
| Verbe | Url | Description |
| :--   | :-- | :--         |
| GET | `https://play.google.com/store/apps/details?id={package}` | Retourne code de status de l'entête (200 ou 404) de la page Google Play associé à l'application Android passé dans l'url. |

<br/><br/>


<!-------------------- Étape 2 -------------------->
# Étape 2
## Exécution
### Syntaxe
```sh
urlDownloader.sh FICHIER_SOURCE [LIGNE_DE_DEBUT]
```

### Description
Récupère l'url de téléchergement des apk gratuits des applications de `FICHIER_SOURCE` qui sont encore disponnibles sur Google Play, à partir de la `LIGNE_DE_DEBUT` (ligne 1 par défaut).

### Exemples
```sh
$ urlDownloader.sh ./data/csv/ApkGooglePlay.csv
$ urlDownloader.sh ./data/csv/ApkGooglePlay.csv 10
```

## Fichiers générés
| Catégorie | Reptoire | Fichiers | Description |
| :--       | :--      | :--      | :--         |
| log | `/var/log/sherlockholms` | `urlDownloader.log` | Journal des événements. |
| temporaire | `/tmp/sherlockholms` | `index.html` | La page HTML qui contient des données (timestamp et token). |
| resultat | `sherlockholms/data/store/url/googleplay` | `GooglePlayStore_errors.csv` | Applications pour lequel une erreur est survenue lors de la recherche. |
| resultat | `sherlockholms/data/store/url/googleplay` | `GooglePlayStore_notfound.csv` | Applications payantes. |
| resultat | `sherlockholms/data/store/url/googleplay` | `GooglePlayStore_success.csv` | Application gratuites. |

## Requête
| Verbe | Url | Description |
| :--   | :-- | :--         |
| GET | `https://apps.evozi.com/apk-downloader` | Retourne un page HTML comprenant un timestamp et un token. |
| POST | `https://api-apk.evozi.com/download` | Retourne en JSON les informations sur le package spécifié dans le formulaire. |

<br/><br/>


<!-------------------- Étape 3 -------------------->
# Étape 3
<span style="background:yellow;color:black;padding:0.3em 0.8em">A FAIRE</span>