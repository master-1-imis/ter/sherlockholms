################################################################################
#	IMPORT FEATURES															   #
################################################################################
source("www/ui/tools.R", local = TRUE)



###############################################################################
#	CONSTANTS																  #
###############################################################################
PREFIX.ID.LEARNING <- "learning"



###############################################################################
#	UI																		  #
###############################################################################
fluidPage(

	#######################################
	#	DATA IMPORT						  #
	#######################################
	# section d'import du jeu de données
	ui_import_data(PREFIX.ID.LEARNING),
	hr(),


	#######################################
	#	LEARNING METHOD					  #
	#######################################
	# section de sélection du modèle d'apprentissage
	fluidRow(
		column(
			width = 12,
			h3("Modèle d'apprentissage"),
			br()
		),
		column(
			width = 12,
			selectInput(
				inputId = paste0(PREFIX.ID.LEARNING, ".method"),
				label = "Méthode",
				choices = c(
					"Forêt d'arbres de décision" = "randomForest",
					"Machine à vecteurs de support ( svm )" = "svm",
					"K Plus proches voisins ( knn )" = "knn"
				),
				selected = "randomForest"
			)
		)
	),
	hr(),
	conditionalPanel(
		condition = paste0("input['", PREFIX.ID.LEARNING, ".method'] == 'knn'"),
		fluidRow(
			column(
				width = 12,
				h3("Paramètres"),
				br()
			),
			column(
				width = 12,
				radioButtons(
					inputId = paste0(PREFIX.ID.LEARNING, ".knn.param.method"),
					label = "Calcul de distance",
					inline = TRUE,
					choices = c(
						"Cosine" = "Cosine",
						"Euclidean" = "Euclidean", 
						"Jaccard" = "Jaccard" 
					),
					selected = "Cosine"
				),
				br()
			),
			column(
				width = 12,
				sliderInput(
					inputId = paste0(PREFIX.ID.LEARNING, ".knn.param.k"),
					label = "Nombre de voisin ( k )",
					min = 1,
					max = 201,
					step = 2,
					value = 31,
					width = "100%"
				)
			)
		)
	),
	conditionalPanel(
		condition = paste0("input['", PREFIX.ID.LEARNING, ".method'] == 'svm'"),
		fluidRow(
			column(
				width = 12,
				h3("Paramètres"),
				br()
			),
			column(
				width = 4,
				selectInput(
					inputId = paste0(PREFIX.ID.LEARNING, ".svm.param.kernel"),
					label = "Kernel",
					choices = c(
						"Linear" = "linear",
						"Polynomial" = "polynomial",
						"Radial Basis Function (RBF)" = "radial",
						"Sigmoid" = "sigmoid"
					),
					selected = "linear"
				),
				br()
			)
		),
		fluidRow(
			column(
				width = 3,
				numericInput(
					inputId = paste0(PREFIX.ID.LEARNING, ".svm.param.cost"),
					label = "Coût",
					value = 1,
					min = 1
				)
			),
			conditionalPanel(
				condition = paste0("input['", PREFIX.ID.LEARNING, ".svm.param.kernel'] !== 'linear'"),
				column(
					width = 3,
					numericInput(
						inputId = paste0(PREFIX.ID.LEARNING, ".svm.param.gamma"),
						label = "Gamma",
						value = 0.5,
						min = 0
					)
				)
			),
			conditionalPanel(
				condition = paste0("['polynomial', 'sigmoid'].includes(input['", PREFIX.ID.LEARNING, ".svm.param.kernel'])"),
				column(
					width = 3,
					numericInput(
						inputId = paste0(PREFIX.ID.LEARNING, ".svm.param.coef"),
						label = "Coefficient",
						value = 0
					)
				)
			),
			conditionalPanel(
				condition = paste0("input['", PREFIX.ID.LEARNING, ".svm.param.kernel'] == 'polynomial'"),
				column(
					width = 3,
					numericInput(
						inputId = paste0(PREFIX.ID.LEARNING, ".svm.param.degree"),
						label = "Degré",
						value = 3,
						min = 0
					)
				)
			)
		)
	),
	conditionalPanel(
		condition = paste0("input['", PREFIX.ID.LEARNING, ".method'] == 'randomForest'"),
		fluidRow(
			column(
				width = 12,
				h3("Paramètres"),
				br()
			),
			column(
				width = 12,
				sliderInput(
					inputId = paste0(PREFIX.ID.LEARNING, ".randomForest.param.nbtree"),
					label = "Nombre d'arbre aléatoire",
					min = 1,
					max = 500,
					step = 1,
					value = 400,
					width = "100%"
				),
				br()
			),
			column(
				width = 12,
				sliderInput(
					inputId = paste0(PREFIX.ID.LEARNING, ".randomForest.param.nbcaracteristic"),
					label = "Nombre de caractéristiques",
					min = 1,
					max = 500,
					step = 1,
					value = 60,
					width = "100%"
				),
			)
		)
	),
	hr(),
	br(),
	fluidRow(
		column(
			width = 12,
			actionButton(
				inputId = paste0(PREFIX.ID.LEARNING, ".validation"),
				label = "Démarer l'apprentissage"
			)
		)
	),
	hr(),


	#######################################
	#	DATA DIAGNOSTIC					  #
	#######################################	
	# affichage des données statistiques du jeu de données
	fluidRow(
		column(
			width = 12,
			h3("Diagnostique du jeu de données")
		),
	),
	br(),
	fluidRow(
		column(
			width = 12,
			tabsetPanel(
				id = paste0(PREFIX.ID.LEARNING, ".diagnostic.tab"),
				type = "tabs",
				
				# onglet de diagnostique des classes
				ui_diagnostic_tab_class(PREFIX.ID.LEARNING),
				
				# onglet de diagnostique des données utilisées pour l'apprentissage
				ui_diagnostic_tab_data(PREFIX.ID.LEARNING),

				# onglet de données statistique de la prédiction
				ui_diagnostic_tab_prediction_checking(PREFIX.ID.LEARNING),

				# onglet de résultat de la prédiction des applications du jeu de données importé
				ui_diagnostic_tab_prediction(PREFIX.ID.LEARNING)
			)
		)
	),
	br()
)