# Sherlock Holms

**date :** 26 janvier 2021 au 15 mai 2021  
**sujet :** [sujet.pdf](sujet.pdf)  
**soutenance :** [soutenance.pdf](soutenance.pdf)  
**rapport :** [rapport.pdf](rapport.pdf) 
<br>
<br>

## Objectif
- Explorer les solutions existantes ainsi que les jeux de données proposés dans les articles pour le traitement et la détection des malwares.

- Proposer et mettre en place une ou plusieurs solutions efficaces pour la détection de malwares en utilisant des jeux de données issues de signatures et / ou d’analyse comportementale de malwares.

- Évaluer les performances des solutions proposées sur des jeux de données d’applications et si possible l’inclure dans un environnement Big Data pour le traitement de grosse masse de données.

<br>
<br>

## Commandes d'installations
```bash
$ git clone https://gitlab.com/master-1-imis/ter/sherlockholms.git
```
<br>
<br>

## Commandes d'exécutions
```bash
$ cd ./sherlockholms/
$ ./script/bash/launcher.sh 
```
<br>
<br>

## Jeux données utilisables

Nom| Contenu| Chemin d'accès 
:- |:-: |:-
Mendeley Data|Benign and Malware|`/data/partial/mendeleydata/feature/All.csv` 
Mendeley Data |Benign|`/data/partial/mendeleydata/feature/Benign.csv` 
Mendeley Data |Malware|`/data/partial/mendeleydata/feature/Malware.csv` 
Figshare|Benign and Malware| `/data/final/figshare/All.csv`
Figshare|Benign| `/data/final/figshare/Benign.csv`
Figshare|Malware| `/data/final/figshare/Malware.csv`

<br>
<br>

## Prérequis logiciel

Logiciel | Version utilisée | Description
:- |:-: | :-
R| 3.6.3 | Pour exécuter le démonstrateur 
Python| 3.8 | Pour exécuter les scripts de filtrage 
Bash| - | Pour exécuter les scripts des  requêtes CURL

<br>
<br>

## Réaliser une contribution

**Syntaxe :**  
`ACTION (catégories): description`

**Actions :**  
`ADD` : ajouter de nouvelles fonctionnalitées   
`MOD` : modifier les fonctionnalitées  
`DEL` : supprimer des foncitonnalitées  
`BUG` : debugger les fonctionnalitées

**Catégories** :  
`src` : code source  
`lib` : gestion des librairies  
`doc` : documentation  
`data` : jeux de données  
`git` : versionnage  
`arch` : architecture

**Description :**  
Une petite description sur les changements réalisés.

**Exemples :** 

```
MOD (git): ignore .idea/ 
ADD (src, data): script pour filtrer le jeu de données  
```


## Contributeurs  

- Arnaud ORLAY (@arnorlay) : Master 1 IMIS Informatique
- Marion JURÉ (@Marionjure) : Master 1 IMIS Informatique
- Nicolas ZHOU (@Zhou_Nicolas) : Master 1 IMIS Informatique