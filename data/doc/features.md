# Features
## Statistic
| # | Category | Number of features | Number of permissions | Number of dangerous protections | Number of dangerous protections with permission | Number of dangerous protections without permission |
| --: | :-- | --: | --: | --: | --: | --: |
|  **1** | Default 						| 62      | <span style="white-space:pre;font-family:monospace">10 <span style="color:#888888">[ 16%]</span></span> | <span style="white-space:pre;font-family:monospace"> 0 <span style="color:#888888">[  0%]</span></span> | <span style="white-space:pre;font-family:monospace"> 0 <span style="color:#888888">[  0%]</span></span> | <span style="white-space:pre;font-family:monospace"> 0 <span style="color:#888888">[  0%]</span></span> |
|  **2** | Development tools 			|  4      | <span style="white-space:pre;font-family:monospace"> 0 <span style="color:#888888">[  0%]</span></span> | <span style="white-space:pre;font-family:monospace"> 4 <span style="color:#888888">[100%]</span></span> | <span style="white-space:pre;font-family:monospace"> 0 <span style="color:#888888">[  0%]</span></span> | <span style="white-space:pre;font-family:monospace"> 4 <span style="color:#888888">[100%]</span></span> |
|  **3** | Hardware controls 			|  6      | <span style="white-space:pre;font-family:monospace"> 1 <span style="color:#888888">[ 16%]</span></span> | <span style="white-space:pre;font-family:monospace"> 3 <span style="color:#888888">[ 50%]</span></span> | <span style="white-space:pre;font-family:monospace"> 1 <span style="color:#888888">[ 33%]</span></span> | <span style="white-space:pre;font-family:monospace"> 2 <span style="color:#888888">[ 67%]</span></span> |
|  **4** | Network communication 		|  9      | <span style="white-space:pre;font-family:monospace"> 4 <span style="color:#888888">[ 44%]</span></span> | <span style="white-space:pre;font-family:monospace"> 4 <span style="color:#888888">[ 44%]</span></span> | <span style="white-space:pre;font-family:monospace"> 2 <span style="color:#888888">[ 50%]</span></span> | <span style="white-space:pre;font-family:monospace"> 2 <span style="color:#888888">[ 50%]</span></span> |
|  **5** | Phone calls 					|  3      | <span style="white-space:pre;font-family:monospace"> 3 <span style="color:#888888">[100%]</span></span> | <span style="white-space:pre;font-family:monospace"> 2 <span style="color:#888888">[ 66%]</span></span> | <span style="white-space:pre;font-family:monospace"> 0 <span style="color:#888888">[  0%]</span></span> | <span style="white-space:pre;font-family:monospace"> 2 <span style="color:#888888">[100%]</span></span> |
|  **6** | Services that cost you money |  2      | <span style="white-space:pre;font-family:monospace"> 2 <span style="color:#888888">[100%]</span></span> | <span style="white-space:pre;font-family:monospace"> 2 <span style="color:#888888">[100%]</span></span> | <span style="white-space:pre;font-family:monospace"> 2 <span style="color:#888888">[100%]</span></span> | <span style="white-space:pre;font-family:monospace"> 0 <span style="color:#888888">[  0%]</span></span> |
|  **7** | Storage 						|  1      | <span style="white-space:pre;font-family:monospace"> 1 <span style="color:#888888">[100%]</span></span> | <span style="white-space:pre;font-family:monospace"> 1 <span style="color:#888888">[100%]</span></span> | <span style="white-space:pre;font-family:monospace"> 1 <span style="color:#888888">[100%]</span></span> | <span style="white-space:pre;font-family:monospace"> 0 <span style="color:#888888">[  0%]</span></span> |
|  **8** | System tools 				| 35      | <span style="white-space:pre;font-family:monospace"> 2 <span style="color:#888888">[  5%]</span></span> | <span style="white-space:pre;font-family:monospace">20 <span style="color:#888888">[ 57%]</span></span> | <span style="white-space:pre;font-family:monospace"> 2 <span style="color:#888888">[ 10%]</span></span> | <span style="white-space:pre;font-family:monospace">18 <span style="color:#888888">[ 90%]</span></span> |
|  **9** | Your accounts 				| 21      | <span style="white-space:pre;font-family:monospace">21 <span style="color:#888888">[100%]</span></span> | <span style="white-space:pre;font-family:monospace">16 <span style="color:#888888">[ 76%]</span></span> | <span style="white-space:pre;font-family:monospace">16 <span style="color:#888888">[100%]</span></span> | <span style="white-space:pre;font-family:monospace"> 0 <span style="color:#888888">[  0%]</span></span> |
| **10** | Your location 				|  4      | <span style="white-space:pre;font-family:monospace"> 4 <span style="color:#888888">[100%]</span></span> | <span style="white-space:pre;font-family:monospace"> 3 <span style="color:#888888">[ 75%]</span></span> | <span style="white-space:pre;font-family:monospace"> 3 <span style="color:#888888">[100%]</span></span> | <span style="white-space:pre;font-family:monospace"> 0 <span style="color:#888888">[  0%]</span></span> |
| **11** | Your messages 				| 14      | <span style="white-space:pre;font-family:monospace"> 5 <span style="color:#888888">[ 35%]</span></span> | <span style="white-space:pre;font-family:monospace">11 <span style="color:#888888">[ 78%]</span></span> | <span style="white-space:pre;font-family:monospace"> 4 <span style="color:#888888">[ 36%]</span></span> | <span style="white-space:pre;font-family:monospace"> 7 <span style="color:#888888">[ 64%]</span></span> |
| **12** | Your personal information 	| 12      | <span style="white-space:pre;font-family:monospace"> 4 <span style="color:#888888">[ 33%]</span></span> | <span style="white-space:pre;font-family:monospace"> 8 <span style="color:#888888">[ 66%]</span></span> | <span style="white-space:pre;font-family:monospace"> 4 <span style="color:#888888">[ 50%]</span></span> | <span style="white-space:pre;font-family:monospace"> 4 <span style="color:#888888">[ 50%]</span></span> |
| **13** | **TOTAL** 					| **173** | <span style="white-space:pre;font-family:monospace;font-weight:bold">57 <span style="color:#888888;font-weight:bold">[ 32%]</span></span> | <span style="white-space:pre;font-family:monospace;font-weight:bold">74 <span style="color:#888888;font-weight:bold">[ 42%]</span></span> | <span style="white-space:pre;font-family:monospace;font-weight:bold">35 <span style="color:#888888;font-weight:bold">[ 47%]</span></span> | <span style="white-space:pre;font-family:monospace;font-weight:bold">39 <span style="color:#888888;font-weight:bold">[ 53%]</span></span> |

<br/><br/>

## Default
| # | Feature | Permission | Protection |
| ---:| :--- | :--: | :--: |
|  **1** | Access DRM content 													| <span>No</span> 																| <span>Normal</span> |
|  **2** | Access Email provider data 											| <span>No</span> 																| <span>Normal</span> |
|  **3** | Access all system downloads 											| <span>No</span> 																| <span>Normal</span> |
|  **4** | Access download manager 												| <span>No</span> 																| <span>Normal</span> |
|  **5** | Advanced download manager functions 									| <span>No</span> 																| <span>Normal</span> |
|  **6** | Audio File Access 													| <span style="background:#ffAE42;color:#FFFFFF;padding:0.3em 0.9em">Yes</span> | <span>Normal</span> |
|  **7** | Install DRM content 													| <span>No</span> 																| <span>Normal</span> |
|  **8** | Modify Google service configuration 									| <span>No</span> 																| <span>Normal</span> |
|  **9** | Modify Google settings 												| <span>No</span> 																| <span>Normal</span> |
| **10** | Move application resources 											| <span>No</span> 																| <span>Normal</span> |
| **11** | Read Google settings 												| <span>No</span> 																| <span>Normal</span> |
| **12** | Send download notifications. 										| <span>No</span> 																| <span>Normal</span> |
| **13** | Voice Search Shortcuts 												| <span>No</span> 																| <span>Normal</span> |
| **14** | access SurfaceFlinger 												| <span>No</span> 																| <span>Normal</span> |
| **15** | access checkin properties 											| <span>No</span> 																| <span>Normal</span> |
| **16** | access the cache filesystem 											| <span>No</span> 																| <span>Normal</span> |
| **17** | access to passwords for Google accounts 								| <span style="background:#ffAE42;color:#FFFFFF;padding:0.3em 0.9em">Yes</span> | <span>Normal</span> |
| **18** | act as an account authenticator 										| <span>No</span> 																| <span>Normal</span> |
| **19** | bind to a wallpaper 													| <span>No</span> 																| <span>Normal</span> |
| **20** | bind to an input method 												| <span>No</span> 																| <span>Normal</span> |
| **21** | change screen orientation 											| <span>No</span> 																| <span>Normal</span> |
| **22** | coarse (network-based) location 										| <span style="background:#ffAE42;color:#FFFFFF;padding:0.3em 0.9em">Yes</span> | <span>Normal</span> |
| **23** | control location update notifications 								| <span style="background:#ffAE42;color:#FFFFFF;padding:0.3em 0.9em">Yes</span> | <span>Normal</span> |
| **24** | control system backup and restore 									| <span>No</span> 																| <span>Normal</span> |
| **25** | delete applications 													| <span>No</span> 																| <span>Normal</span> |
| **26** | delete other applications' caches 									| <span>No</span> 																| <span>Normal</span> |
| **27** | delete other applications' data 										| <span>No</span> 																| <span>Normal</span> |
| **28** | directly call any phone numbers 										| <span>No</span> 																| <span>Normal</span> |
| **29** | directly install applications 										| <span>No</span> 																| <span>Normal</span> |
| **30** | disable or modify status bar 										| <span>No</span> 																| <span>Normal</span> |
| **31** | discover known accounts 												| <span style="background:#ffAE42;color:#FFFFFF;padding:0.3em 0.9em">Yes</span> | <span>Normal</span> |
| **32** | display unauthorized windows 										| <span>No</span> 																| <span>Normal</span> |
| **33** | enable or disable application components 							| <span>No</span> 																| <span>Normal</span> |
| **34** | force application to close 											| <span>No</span> 																| <span>Normal</span> |
| **35** | force device reboot 													| <span>No</span> 																| <span>Normal</span> |
| **36** | full Internet access 												| <span>No</span> 																| <span>Normal</span> |
| **37** | interact with a device admin 										| <span>No</span> 																| <span>Normal</span> |
| **38** | manage application tokens 											| <span>No</span> 																| <span>Normal</span> |
| **39** | mock location sources for testing 									| <span style="background:#ffAE42;color:#FFFFFF;padding:0.3em 0.9em">Yes</span> | <span>Normal</span> |
| **40** | modify battery statistics 											| <span>No</span> 																| <span>Normal</span> |
| **41** | modify secure system settings 										| <span>No</span> 																| <span>Normal</span> |
| **42** | modify the Google services map 										| <span>No</span> 																| <span>Normal</span> |
| **43** | modify/delete USB storage contents modify/delete SD card contents 	| <span style="background:#ffAE42;color:#FFFFFF;padding:0.3em 0.9em">Yes</span> | <span>Normal</span> |
| **44** | monitor and control all application launching 						| <span>No</span> 																| <span>Normal</span> |
| **45** | partial shutdown 													| <span>No</span> 																| <span>Normal</span> |
| **46** | permanently disable device 											| <span>No</span> 																| <span>Normal</span> |
| **47** | permission to install a location provider 							| <span style="background:#ffAE42;color:#FFFFFF;padding:0.3em 0.9em">Yes</span> | <span>Normal</span> |
| **48** | power device on or off 												| <span>No</span> 																| <span>Normal</span> |
| **49** | press keys and control buttons 										| <span>No</span> 																| <span>Normal</span> |
| **50** | prevent app switches 												| <span>No</span> 																| <span>Normal</span> |
| **51** | read frame buffer 													| <span>No</span> 																| <span>Normal</span> |
| **52** | read instant messages 												| <span>No</span> 																| <span>Normal</span> |
| **53** | read phone state and identity 										| <span style="background:#ffAE42;color:#FFFFFF;padding:0.3em 0.9em">Yes</span> | <span>Normal</span> |
| **54** | record what you type and actions you take 							| <span>No</span> 																| <span>Normal</span> |
| **55** | reset system to factory defaults 									| <span>No</span> 																| <span>Normal</span> |
| **56** | run in factory test mode 											| <span>No</span> 																| <span>Normal</span> |
| **57** | set time 															| <span>No</span> 																| <span>Normal</span> |
| **58** | set wallpaper size hints 											| <span>No</span> 																| <span>Normal</span> |
| **59** | start IM service 													| <span>No</span> 																| <span>Normal</span> |
| **60** | update component usage statistics 									| <span>No</span> 																| <span>Normal</span> |
| **61** | write contact data 													| <span style="background:#ffAE42;color:#FFFFFF;padding:0.3em 0.9em">Yes</span> | <span>Normal</span> |
| **62** | write instant messages 												| <span>No</span> 																| <span>Normal</span> |

<br/><br/>

## Development tools
| # | Feature | Permission | Protection |
| ---:| :--- | :--: | :--: |
|  **1** | enable application debugging 			| <span>No</span> | <span style="background:#FF5042;color:#FFFFFF;padding:0.3em 0.9em">Dangerous</span> |
|  **2** | limit number of running processes 		| <span>No</span> | <span style="background:#FF5042;color:#FFFFFF;padding:0.3em 0.9em">Dangerous</span> |
|  **3** | make all background applications close 	| <span>No</span> | <span style="background:#FF5042;color:#FFFFFF;padding:0.3em 0.9em">Dangerous</span> |
|  **4** | send Linux signals to applications 		| <span>No</span> | <span style="background:#FF5042;color:#FFFFFF;padding:0.3em 0.9em">Dangerous</span> |

<br/><br/>

## Hardware controls
| # | Feature | Permission | Protection |
| ---:| :--- | :--: | :--: |
|  **1** | change your audio settings	| <span>No</span> 																| <span style="background:#FF5042;color:#FFFFFF;padding:0.3em 0.9em">Dangerous</span> |
|  **2** | control flashlight			| <span>No</span> 																| <span>Normal</span> 																  |
|  **3** | control vibrator				| <span>No</span> 																| <span>Normal</span> 																  |
|  **4** | record audio					| <span style="background:#ffAE42;color:#FFFFFF;padding:0.3em 0.9em">Yes</span> | <span style="background:#FF5042;color:#FFFFFF;padding:0.3em 0.9em">Dangerous</span> |
|  **5** | take pictures and videos		| <span>No</span> 																| <span style="background:#FF5042;color:#FFFFFF;padding:0.3em 0.9em">Dangerous</span> |
|  **6** | test hardware				| <span>No</span> 																| <span>Normal</span> 																  |

<br/><br/>

## Network communication
| # | Feature | Permission | Protection |
| ---:| :--- | :--: | :--: |
|  **1** | Broadcast data messages to applications	| <span>No</span> 																| <span>Normal</span> 																  |
|  **2** | control Near Field Communication			| <span>No</span> 																| <span style="background:#FF5042;color:#FFFFFF;padding:0.3em 0.9em">Dangerous</span> |
|  **3** | create Bluetooth connections				| <span>No</span> 																| <span style="background:#FF5042;color:#FFFFFF;padding:0.3em 0.9em">Dangerous</span> |
|  **4** | download files without notification		| <span>No</span> 																| <span>Normal</span> 																  |
|  **5** | full Internet access						| <span style="background:#ffAE42;color:#FFFFFF;padding:0.3em 0.9em">Yes</span> | <span style="background:#FF5042;color:#FFFFFF;padding:0.3em 0.9em">Dangerous</span> |
|  **6** | make/receive Internet calls				| <span style="background:#ffAE42;color:#FFFFFF;padding:0.3em 0.9em">Yes</span> | <span style="background:#FF5042;color:#FFFFFF;padding:0.3em 0.9em">Dangerous</span> |
|  **7** | receive data from Internet				| <span>No</span> 																| <span>Normal</span> 																  |
|  **8** | view Wi-Fi state							| <span style="background:#ffAE42;color:#FFFFFF;padding:0.3em 0.9em">Yes</span> | <span>Normal</span> 																  |
|  **9** | view network state						| <span style="background:#ffAE42;color:#FFFFFF;padding:0.3em 0.9em">Yes</span> | <span>Normal</span> 																  |

<br/><br/>

## Phone calls
| # | Feature | Permission | Protection |
| ---:| :--- | :--: | :--: |
|  **1** | intercept outgoing calls			| <span style="background:#ffAE42;color:#FFFFFF;padding:0.3em 0.9em">Yes</span> | <span style="background:#FF5042;color:#FFFFFF;padding:0.3em 0.9em">Dangerous</span> |
|  **2** | modify phone state				| <span style="background:#ffAE42;color:#FFFFFF;padding:0.3em 0.9em">Yes</span> | <span>Normal</span> 																  |
|  **3** | read phone state and identity	| <span style="background:#ffAE42;color:#FFFFFF;padding:0.3em 0.9em">Yes</span> | <span style="background:#FF5042;color:#FFFFFF;padding:0.3em 0.9em">Dangerous</span> |

<br/><br/>

## Services that cost you money
| # | Feature | Permission | Protection |
| ---:| :--- | :--: | :--: |
|  **1** | directly call phone numbers	| <span style="background:#ffAE42;color:#FFFFFF;padding:0.3em 0.9em">Yes</span> | <span style="background:#FF5042;color:#FFFFFF;padding:0.3em 0.9em">Dangerous</span> |
|  **2** | send SMS messages			| <span style="background:#ffAE42;color:#FFFFFF;padding:0.3em 0.9em">Yes</span> | <span style="background:#FF5042;color:#FFFFFF;padding:0.3em 0.9em">Dangerous</span> |

<br/><br/>

## Storage
| # | Feature | Permission | Protection |
| ---:| :--- | :--: | :--: |
|  **1** | modify/delete USB storage contents modify/delete SD card contents	| <span style="background:#ffAE42;color:#FFFFFF;padding:0.3em 0.9em">Yes</span> | <span style="background:#FF5042;color:#FFFFFF;padding:0.3em 0.9em">Dangerous</span> |

<br/><br/>

## System tools
| # | Feature | Permission | Protection |
| ---:| :--- | :--: | :--: |
|  **1** | allow Wi-Fi Multicast reception			| <span>No</span> 																| <span style="background:#FF5042;color:#FFFFFF;padding:0.3em 0.9em">Dangerous</span> |
|  **2** | automatically start at boot				| <span>No</span> 																| <span>Normal</span> 																  |
|  **3** | bluetooth administration					| <span>No</span> 																| <span style="background:#FF5042;color:#FFFFFF;padding:0.3em 0.9em">Dangerous</span> |
|  **4** | change Wi-Fi state						| <span style="background:#ffAE42;color:#FFFFFF;padding:0.3em 0.9em">Yes</span> | <span style="background:#FF5042;color:#FFFFFF;padding:0.3em 0.9em">Dangerous</span> |
|  **5** | change background data usage setting		| <span>No</span> 																| <span>Normal</span> 																  |
|  **6** | change network connectivity				| <span>No</span> 																| <span style="background:#FF5042;color:#FFFFFF;padding:0.3em 0.9em">Dangerous</span> |
|  **7** | change your UI settings					| <span>No</span> 																| <span style="background:#FF5042;color:#FFFFFF;padding:0.3em 0.9em">Dangerous</span> |
|  **8** | delete all application cache data		| <span>No</span> 																| <span style="background:#FF5042;color:#FFFFFF;padding:0.3em 0.9em">Dangerous</span> |
|  **9** | disable keylock							| <span>No</span> 																| <span style="background:#FF5042;color:#FFFFFF;padding:0.3em 0.9em">Dangerous</span> |
| **10** | display system-level alerts				| <span>No</span> 																| <span style="background:#FF5042;color:#FFFFFF;padding:0.3em 0.9em">Dangerous</span> |
| **11** | expand/collapse status bar				| <span>No</span> 																| <span>Normal</span> 																  |
| **12** | force stop other applications			| <span>No</span> 																| <span>Normal</span> 																  |
| **13** | format external storage					| <span style="background:#ffAE42;color:#FFFFFF;padding:0.3em 0.9em">Yes</span> | <span style="background:#FF5042;color:#FFFFFF;padding:0.3em 0.9em">Dangerous</span> |
| **14** | kill background processes				| <span>No</span> 																| <span>Normal</span> 																  |
| **15** | make application always run				| <span>No</span> 																| <span style="background:#FF5042;color:#FFFFFF;padding:0.3em 0.9em">Dangerous</span> |
| **16** | measure application storage space		| <span>No</span> 																| <span>Normal</span> 																  |
| **17** | modify global animation speed			| <span>No</span> 																| <span style="background:#FF5042;color:#FFFFFF;padding:0.3em 0.9em">Dangerous</span> |
| **18** | modify global system settings			| <span>No</span> 																| <span style="background:#FF5042;color:#FFFFFF;padding:0.3em 0.9em">Dangerous</span> |
| **19** | mount and unmount filesystems			| <span>No</span> 																| <span style="background:#FF5042;color:#FFFFFF;padding:0.3em 0.9em">Dangerous</span> |
| **20** | prevent device from sleeping				| <span>No</span> 																| <span style="background:#FF5042;color:#FFFFFF;padding:0.3em 0.9em">Dangerous</span> |
| **21** | read subscribed feeds					| <span>No</span> 																| <span>Normal</span> 																  |
| **22** | read sync settings						| <span>No</span> 																| <span>Normal</span> 																  |
| **23** | read sync statistics						| <span>No</span> 																| <span>Normal</span> 																  |
| **24** | read/write to resources owned by diag	| <span>No</span> 																| <span>Normal</span> 																  |
| **25** | reorder running applications				| <span>No</span> 																| <span style="background:#FF5042;color:#FFFFFF;padding:0.3em 0.9em">Dangerous</span> |
| **26** | retrieve running applications			| <span>No</span> 																| <span style="background:#FF5042;color:#FFFFFF;padding:0.3em 0.9em">Dangerous</span> |
| **27** | send package removed broadcast			| <span>No</span> 																| <span>Normal</span> 																  |
| **28** | send sticky broadcast					| <span>No</span> 																| <span>Normal</span> 																  |
| **29** | set preferred applications				| <span>No</span> 																| <span>Normal</span> 																  |
| **30** | set time zone							| <span>No</span> 																| <span style="background:#FF5042;color:#FFFFFF;padding:0.3em 0.9em">Dangerous</span> |
| **31** | set wallpaper							| <span>No</span> 																| <span>Normal</span> 																  |
| **32** | set wallpaper size hints					| <span>No</span> 																| <span>Normal</span> 																  |
| **33** | write Access Point Name settings			| <span>No</span> 																| <span style="background:#FF5042;color:#FFFFFF;padding:0.3em 0.9em">Dangerous</span> |
| **34** | write subscribed feeds					| <span>No</span> 																| <span style="background:#FF5042;color:#FFFFFF;padding:0.3em 0.9em">Dangerous</span> |
| **35** | write sync settings						| <span>No</span> 																| <span style="background:#FF5042;color:#FFFFFF;padding:0.3em 0.9em">Dangerous</span> |

<br/><br/>

## Your accounts
| # | Feature | Permission | Protection |
| ---:| :--- | :--: | :--: |
|  **1** | Blogger 											| <span style="background:#ffAE42;color:#FFFFFF;padding:0.3em 0.9em">Yes</span> | <span style="background:#FF5042;color:#FFFFFF;padding:0.3em 0.9em">Dangerous</span> |
|  **2** | Google App Engine 								| <span style="background:#ffAE42;color:#FFFFFF;padding:0.3em 0.9em">Yes</span> | <span style="background:#FF5042;color:#FFFFFF;padding:0.3em 0.9em">Dangerous</span> |
|  **3** | Google Docs 										| <span style="background:#ffAE42;color:#FFFFFF;padding:0.3em 0.9em">Yes</span> | <span style="background:#FF5042;color:#FFFFFF;padding:0.3em 0.9em">Dangerous</span> |
|  **4** | Google Finance 									| <span style="background:#ffAE42;color:#FFFFFF;padding:0.3em 0.9em">Yes</span> | <span style="background:#FF5042;color:#FFFFFF;padding:0.3em 0.9em">Dangerous</span> |
|  **5** | Google Maps 										| <span style="background:#ffAE42;color:#FFFFFF;padding:0.3em 0.9em">Yes</span> | <span style="background:#FF5042;color:#FFFFFF;padding:0.3em 0.9em">Dangerous</span> |
|  **6** | Google Spreadsheets 								| <span style="background:#ffAE42;color:#FFFFFF;padding:0.3em 0.9em">Yes</span> | <span style="background:#FF5042;color:#FFFFFF;padding:0.3em 0.9em">Dangerous</span> |
|  **7** | Google Voice 									| <span style="background:#ffAE42;color:#FFFFFF;padding:0.3em 0.9em">Yes</span> | <span style="background:#FF5042;color:#FFFFFF;padding:0.3em 0.9em">Dangerous</span> |
|  **8** | Google mail 										| <span style="background:#ffAE42;color:#FFFFFF;padding:0.3em 0.9em">Yes</span> | <span style="background:#FF5042;color:#FFFFFF;padding:0.3em 0.9em">Dangerous</span> |
|  **9** | Picasa Web Albums 								| <span style="background:#ffAE42;color:#FFFFFF;padding:0.3em 0.9em">Yes</span> | <span style="background:#FF5042;color:#FFFFFF;padding:0.3em 0.9em">Dangerous</span> |
| **10** | YouTube 											| <span style="background:#ffAE42;color:#FFFFFF;padding:0.3em 0.9em">Yes</span> | <span style="background:#FF5042;color:#FFFFFF;padding:0.3em 0.9em">Dangerous</span> |
| **11** | YouTube usernames 								| <span style="background:#ffAE42;color:#FFFFFF;padding:0.3em 0.9em">Yes</span> | <span style="background:#FF5042;color:#FFFFFF;padding:0.3em 0.9em">Dangerous</span> |
| **12** | access all Google services 						| <span style="background:#ffAE42;color:#FFFFFF;padding:0.3em 0.9em">Yes</span> | <span>Normal</span> 																  |
| **13** | access other Google services 					| <span style="background:#ffAE42;color:#FFFFFF;padding:0.3em 0.9em">Yes</span> | <span style="background:#FF5042;color:#FFFFFF;padding:0.3em 0.9em">Dangerous</span> |
| **14** | act as an account authenticator 					| <span style="background:#ffAE42;color:#FFFFFF;padding:0.3em 0.9em">Yes</span> | <span style="background:#FF5042;color:#FFFFFF;padding:0.3em 0.9em">Dangerous</span> |
| **15** | act as the AccountManagerService 				| <span style="background:#ffAE42;color:#FFFFFF;padding:0.3em 0.9em">Yes</span> | <span>Normal</span> 																  |
| **16** | contacts data in Google accounts 				| <span style="background:#ffAE42;color:#FFFFFF;padding:0.3em 0.9em">Yes</span> | <span style="background:#FF5042;color:#FFFFFF;padding:0.3em 0.9em">Dangerous</span> |
| **17** | discover known accounts 							| <span style="background:#ffAE42;color:#FFFFFF;padding:0.3em 0.9em">Yes</span> | <span>Normal</span> 																  |
| **18** | manage the accounts list 						| <span style="background:#ffAE42;color:#FFFFFF;padding:0.3em 0.9em">Yes</span> | <span style="background:#FF5042;color:#FFFFFF;padding:0.3em 0.9em">Dangerous</span> |
| **19** | read Google service configuration 				| <span style="background:#ffAE42;color:#FFFFFF;padding:0.3em 0.9em">Yes</span> | <span>Normal</span> 																  |
| **20** | use the authentication credentials of an account | <span style="background:#ffAE42;color:#FFFFFF;padding:0.3em 0.9em">Yes</span> | <span style="background:#FF5042;color:#FFFFFF;padding:0.3em 0.9em">Dangerous</span> |
| **21** | view configured accounts 						| <span style="background:#ffAE42;color:#FFFFFF;padding:0.3em 0.9em">Yes</span> | <span>Normal</span> 																  |

<br/><br/>

## Your location
| # | Feature | Permission | Protection |
| ---:| :--- | :--: | :--: |
|  **1** | access extra location provider commands 	| <span style="background:#ffAE42;color:#FFFFFF;padding:0.3em 0.9em">Yes</span> | <span>Normal</span> 																  |
|  **2** | coarse (network-based) location 			| <span style="background:#ffAE42;color:#FFFFFF;padding:0.3em 0.9em">Yes</span> | <span style="background:#FF5042;color:#FFFFFF;padding:0.3em 0.9em">Dangerous</span> |
|  **3** | fine (GPS) location 						| <span style="background:#ffAE42;color:#FFFFFF;padding:0.3em 0.9em">Yes</span> | <span style="background:#FF5042;color:#FFFFFF;padding:0.3em 0.9em">Dangerous</span> |
|  **4** | mock location sources for testing 		| <span style="background:#ffAE42;color:#FFFFFF;padding:0.3em 0.9em">Yes</span> | <span style="background:#FF5042;color:#FFFFFF;padding:0.3em 0.9em">Dangerous</span> |

<br/><br/>

## Your messages
| # | Feature | Permission | Protection |
| ---:| :--- | :--: | :--: |
|  **1** | Read Email attachments 			| <span>No</span> 																| <span style="background:#FF5042;color:#FFFFFF;padding:0.3em 0.9em">Dangerous</span> |
|  **2** | Send Gmail 						| <span>No</span> 																| <span>Normal</span> 																  |
|  **3** | edit SMS or MMS 					| <span style="background:#ffAE42;color:#FFFFFF;padding:0.3em 0.9em">Yes</span> | <span style="background:#FF5042;color:#FFFFFF;padding:0.3em 0.9em">Dangerous</span> |
|  **4** | modify Gmail 					| <span>No</span> 																| <span style="background:#FF5042;color:#FFFFFF;padding:0.3em 0.9em">Dangerous</span> |
|  **5** | read Gmail 						| <span>No</span> 																| <span style="background:#FF5042;color:#FFFFFF;padding:0.3em 0.9em">Dangerous</span> |
|  **6** | read Gmail attachment previews 	| <span>No</span> 																| <span style="background:#FF5042;color:#FFFFFF;padding:0.3em 0.9em">Dangerous</span> |
|  **7** | read SMS or MMS 					| <span style="background:#ffAE42;color:#FFFFFF;padding:0.3em 0.9em">Yes</span> | <span style="background:#FF5042;color:#FFFFFF;padding:0.3em 0.9em">Dangerous</span> |
|  **8** | read instant messages 			| <span>No</span> 																| <span style="background:#FF5042;color:#FFFFFF;padding:0.3em 0.9em">Dangerous</span> |
|  **9** | receive MMS 						| <span>No</span> 																| <span style="background:#FF5042;color:#FFFFFF;padding:0.3em 0.9em">Dangerous</span> |
| **10** | receive SMS 						| <span style="background:#ffAE42;color:#FFFFFF;padding:0.3em 0.9em">Yes</span> | <span style="background:#FF5042;color:#FFFFFF;padding:0.3em 0.9em">Dangerous</span> |
| **11** | receive WAP 						| <span style="background:#ffAE42;color:#FFFFFF;padding:0.3em 0.9em">Yes</span> | <span style="background:#FF5042;color:#FFFFFF;padding:0.3em 0.9em">Dangerous</span> |
| **12** | send SMS-received broadcast 		| <span style="background:#ffAE42;color:#FFFFFF;padding:0.3em 0.9em">Yes</span> | <span>Normal</span> 																  |
| **13** | send WAP-PUSH-received broadcast | <span>No</span> 																| <span>Normal</span> 																  |
| **14** | write instant messages 			| <span>No</span> 																| <span style="background:#FF5042;color:#FFFFFF;padding:0.3em 0.9em">Dangerous</span> |

<br/><br/>

## Your personal information
| # | Feature | Permission | Protection |
| ---:| :--- | :--: | :--: |
|  **1** | add or modify calendar events and send email to guests 	| <span style="background:#ffAE42;color:#FFFFFF;padding:0.3em 0.9em">Yes</span> | <span style="background:#FF5042;color:#FFFFFF;padding:0.3em 0.9em">Dangerous</span> |
|  **2** | choose widgets 											| <span>No</span> 																| <span>Normal</span> 																  |
|  **3** | read Browser's history and bookmarks 					| <span>No</span> 																| <span style="background:#FF5042;color:#FFFFFF;padding:0.3em 0.9em">Dangerous</span> |
|  **4** | read calendar events 									| <span style="background:#ffAE42;color:#FFFFFF;padding:0.3em 0.9em">Yes</span> | <span style="background:#FF5042;color:#FFFFFF;padding:0.3em 0.9em">Dangerous</span> |
|  **5** | read contact data 										| <span style="background:#ffAE42;color:#FFFFFF;padding:0.3em 0.9em">Yes</span> | <span style="background:#FF5042;color:#FFFFFF;padding:0.3em 0.9em">Dangerous</span> |
|  **6** | read sensitive log data 									| <span>No</span> 																| <span style="background:#FF5042;color:#FFFFFF;padding:0.3em 0.9em">Dangerous</span> |
|  **7** | read user defined dictionary 							| <span>No</span> 																| <span style="background:#FF5042;color:#FFFFFF;padding:0.3em 0.9em">Dangerous</span> |
|  **8** | retrieve system internal state 							| <span>No</span> 																| <span>Normal</span> 																  |
|  **9** | set alarm in alarm clock 								| <span>No</span> 																| <span>Normal</span> 																  |
| **10** | write Browser's history and bookmarks 					| <span>No</span> 																| <span style="background:#FF5042;color:#FFFFFF;padding:0.3em 0.9em">Dangerous</span> |
| **11** | write contact data 										| <span style="background:#ffAE42;color:#FFFFFF;padding:0.3em 0.9em">Yes</span> | <span style="background:#FF5042;color:#FFFFFF;padding:0.3em 0.9em">Dangerous</span> |
| **12** | write to user defined dictionary 						| <span>No</span> 																| <span>Normal</span> 																  |